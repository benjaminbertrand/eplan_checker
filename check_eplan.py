import sys
import pandas as pd


def convert_to_set(df, column):
    """Convert the given DataFrame column to a set

    - Null values are discarded (warning is printed)
    - All elements are converted to string in uppercase
      to be able to compare them
    """
    # Check if there are empty values
    null_values = df[df[column].isnull()]
    if len(null_values) > 0:
        print('WARNING: Null value(s) found:')
        print(null_values)
    filtered = df[df[column].notnull()]
    return set(filtered[column].str.upper().tolist())


def main():
    if len(sys.argv) != 4:
        print(f'Usage: {sys.argv[0]} eplan.xls ccdb.xlsx naming.xlsx')
        sys.exit(1)
    eplan_xls, ccdb_xls, naming_xls = sys.argv[1:]
    eplan_df = pd.read_excel(eplan_xls, header=4)
    ccdb_df = pd.read_excel(ccdb_xls, header=7, skiprows=[8])
    naming_df = pd.read_excel(naming_xls, header=0, sheetname='Device Names')
    eplan_names = convert_to_set(eplan_df, 'ESS-name')
    cables_names = convert_to_set(ccdb_df, 'CABLE NAME')
    devices_names = convert_to_set(naming_df, 'Device Name')
    cables_devices_names = cables_names.copy()
    cables_devices_names.update(devices_names)
    names_not_in_eplan = cables_devices_names.difference(eplan_names)
    print('----------------------------------------------')
    print(f'Number of names not in Eplan: {len(names_not_in_eplan)}')
    print('\n'.join(sorted(names_not_in_eplan)))
    names_only_in_eplan = eplan_names.difference(cables_devices_names)
    print('----------------------------------------------')
    print(f'Number of names only in Eplan: {len(names_only_in_eplan)}')
    print('\n'.join(sorted(names_only_in_eplan)))
    print('----------------------------------------------')


if __name__ == '__main__':
    main()
